program GaussLegendre
    
    use Legendre_lib
    implicit none
    
    integer(4), parameter :: n=9
    
    call MakeQuad(n)
   
end program GaussLegendre